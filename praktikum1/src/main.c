#include <stdio.h>

// Task 1: Fahrenheit to Celsius converter

float convert_to_celsius(float fahrenheit) 
{
    return (5*(fahrenheit-32))/9;
}

void task_1() 
{
    float celsius;
    int fahrenheit;
    (void)printf("Fahrenheit    Celsius\n");
    (void)printf("---------------------\n");
    for(fahrenheit = -100; fahrenheit <= 200; fahrenheit++) {
        celsius = convert_to_celsius(fahrenheit);
        (void)printf("     %d    %.2f\n", fahrenheit, celsius);
    }
}

// Task 2: Counter
void task_2() 
{
    char read;
    int chars = 0;
    int words = 0;
    // if last char was a char, set it to 1
    // if it was a empty space, set it to 0
    // this prevents, that multiple spaces count as multiple words
    int lastChar = 0;
    while(read != '\n') {
        read = getchar();
        if(isspace(read)){
            // only count as word, if last char was in fact a char and no whitespace
            if(lastChar == 1) {
                words = words + 1;
                lastChar = 0;
            }
        } else {
            chars = chars + 1; 
            lastChar = 1;
        }
    }
    (void)printf("chars: %d words: %d", chars, words);
}

int main(void) 
{
    task_1();
    task_2();

    return 0; 
}
